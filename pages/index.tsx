import Head from 'next/head';
import Image from 'next/image';
import styles from '../styles/Home.module.css';

export default function Home() {
  return (
    <div className={styles.container}>
      <div style={{ border: 'red solid 3px' }}>
        <span className='text-white text-9xl'> Three commit</span>
      </div>
    </div>
  );
}
